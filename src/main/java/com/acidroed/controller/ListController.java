package com.acidroed.controller;

import com.acidroed.entity.ListModel;
import com.acidroed.service.ListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value ="/lists")
public class ListController {

    @Autowired
    ListService listService;


    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addList(@RequestBody ListModel list){
        listService.addList(list);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateList(@RequestBody ListModel list){
        listService.updateList(list);
    }

    @RequestMapping(value ="/{id}", method = RequestMethod.GET)
    public ListModel getListById(@PathVariable("id") long id){
        return listService.getListById(id);
    }

    @RequestMapping(value ="/{id}", method = RequestMethod.DELETE)
    public void deleteListById(@PathVariable("id") long id){
        listService.deleteListById(id);
    }
}
