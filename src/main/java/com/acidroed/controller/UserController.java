package com.acidroed.controller;

import com.acidroed.entity.ListModel;
import com.acidroed.entity.UserModel;
import com.acidroed.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value ="/users")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<UserModel> getAllUsers(){
        return userService.getAllUsers();
    }

    @RequestMapping(value ="/{id}/lists", method = RequestMethod.GET)
    public Collection<ListModel> getAllUsersLists(@PathVariable("id") long userId){
        return userService.getAllUsersLists(userId);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addUser(@RequestBody UserModel user){
        userService.addUser(user);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateUser(@RequestBody UserModel user){
        userService.updateUser(user);
    }

    @RequestMapping(value ="/{id}", method = RequestMethod.GET)
    public UserModel getUserById(@PathVariable("id") long id){
        return userService.getUserById(id);
    }

    @RequestMapping(value ="/{id}", method = RequestMethod.DELETE)
    public void deleteUserById(@PathVariable("id") long id){
        userService.deleteUserById(id);
    }
}
