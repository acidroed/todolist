package com.acidroed.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;


@Entity
@Table(name = "lists", schema = "public", catalog = "todolist")
public class ListModel {
    private long listid;
    private Integer userid;
    private String listname;
    private Timestamp startdate;
    private Timestamp closedate;
    private Timestamp expirationdate;
    private boolean isactive;
    private boolean isdone;
    private Collection<ItemModel> itemsByListId;
    private UserModel userByUserId;


    public ListModel updateList(ListModel updatedList) {
        this.listname = updatedList.getListname();
        this.startdate = updatedList.startdate;
        this.closedate = updatedList.closedate;
        this.expirationdate = updatedList.expirationdate;
        this.isactive = updatedList.isactive;
        this.isdone = updatedList.isdone;
        return this;
    }


    @Id
    @GeneratedValue(strategy= GenerationType.TABLE)
    @Column(name = "listid", nullable = false)
    public long getListid() {
        return listid;
    }

    public void setListid(long listid) {
        this.listid = listid;
    }

    @Basic
    @Column(name = "userid", nullable = true)
    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    @Basic
    @Column(name = "listname", nullable = false, length = 255)
    public String getListname() {
        return listname;
    }

    public void setListname(String listname) {
        this.listname = listname;
    }

    @Basic
    @Column(name = "startdate", nullable = true)
    public Timestamp getStartdate() {
        return startdate;
    }

    public void setStartdate(Timestamp startdate) {
        this.startdate = startdate;
    }

    @Basic
    @Column(name = "closedate", nullable = true)
    public Timestamp getClosedate() {
        return closedate;
    }

    public void setClosedate(Timestamp closedate) {
        this.closedate = closedate;
    }

    @Basic
    @Column(name = "expirationdate", nullable = true)
    public Timestamp getExpirationdate() {
        return expirationdate;
    }

    public void setExpirationdate(Timestamp expirationdate) {
        this.expirationdate = expirationdate;
    }

    @Basic
    @Column(name = "isactive", nullable = false)
    public boolean isIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }

    @Basic
    @Column(name = "isdone", nullable = false)
    public boolean isIsdone() {
        return isdone;
    }

    public void setIsdone(boolean isdone) {
        this.isdone = isdone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ListModel that = (ListModel) o;

        if (listid != that.listid) return false;
        if (isactive != that.isactive) return false;
        if (isdone != that.isdone) return false;
        if (userid != null ? !userid.equals(that.userid) : that.userid != null) return false;
        if (listname != null ? !listname.equals(that.listname) : that.listname != null) return false;
        if (startdate != null ? !startdate.equals(that.startdate) : that.startdate != null) return false;
        if (closedate != null ? !closedate.equals(that.closedate) : that.closedate != null) return false;
        if (expirationdate != null ? !expirationdate.equals(that.expirationdate) : that.expirationdate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (listid ^ (listid >>> 32));
        result = 31 * result + (userid != null ? userid.hashCode() : 0);
        result = 31 * result + (listname != null ? listname.hashCode() : 0);
        result = 31 * result + (startdate != null ? startdate.hashCode() : 0);
        result = 31 * result + (closedate != null ? closedate.hashCode() : 0);
        result = 31 * result + (expirationdate != null ? expirationdate.hashCode() : 0);
        result = 31 * result + (isactive ? 1 : 0);
        result = 31 * result + (isdone ? 1 : 0);
        return result;
    }

    @OneToMany(mappedBy = "listModelByListId", fetch=FetchType.LAZY)
    @Cascade({CascadeType.ALL})
    @JsonManagedReference
    public Collection<ItemModel> getItemsByListId() {
        return itemsByListId;
    }

    public void setItemsByListId(Collection<ItemModel> itemsByListId) {
        this.itemsByListId = itemsByListId;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "userid", referencedColumnName = "userid", insertable = false, updatable = false)
    @JsonBackReference
    public UserModel getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(UserModel userByUserId) {
        this.userByUserId = userByUserId;
    }
}
