package com.acidroed.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "items", schema = "public", catalog = "todolist")
public class ItemModel {
    private long itemid;
    private Integer listid;
    private String description;
    private Timestamp creationdate;
    private Timestamp donedate;
    private boolean isdone;
    private ListModel listModelByListId;

    @Id
    @GeneratedValue(strategy= GenerationType.TABLE)
    @Column(name = "itemid", nullable = false)
    public long getItemid() {
        return itemid;
    }

    public void setItemid(long itemid) {
        this.itemid = itemid;
    }

    @Basic
    @Column(name = "listid", nullable = true)
    public Integer getListid() {
        return listid;
    }

    public void setListid(Integer listid) {
        this.listid = listid;
    }

    @Basic
    @Column(name = "description", nullable = false, length = 255)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "creationdate", nullable = true)
    public Timestamp getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(Timestamp creationdate) {
        this.creationdate = creationdate;
    }

    @Basic
    @Column(name = "donedate", nullable = true)
    public Timestamp getDonedate() {
        return donedate;
    }

    public void setDonedate(Timestamp donedate) {
        this.donedate = donedate;
    }

    @Basic
    @Column(name = "isdone", nullable = false)
    public boolean isIsdone() {
        return isdone;
    }

    public void setIsdone(boolean isdone) {
        this.isdone = isdone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemModel that = (ItemModel) o;

        if (itemid != that.itemid) return false;
        if (isdone != that.isdone) return false;
        if (listid != null ? !listid.equals(that.listid) : that.listid != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (creationdate != null ? !creationdate.equals(that.creationdate) : that.creationdate != null) return false;
        if (donedate != null ? !donedate.equals(that.donedate) : that.donedate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (itemid ^ (itemid >>> 32));
        result = 31 * result + (listid != null ? listid.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (creationdate != null ? creationdate.hashCode() : 0);
        result = 31 * result + (donedate != null ? donedate.hashCode() : 0);
        result = 31 * result + (isdone ? 1 : 0);
        return result;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "listid", referencedColumnName = "listid", insertable = false, updatable = false)
    @JsonBackReference
    public ListModel getListModelByListId() {
        return listModelByListId;
    }

    public void setListModelByListId(ListModel listModelByListId) {
        this.listModelByListId = listModelByListId;
    }
}
