package com.acidroed.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "users", schema = "public", catalog = "todolist")
public class UserModel {
    private long userid;
    private String login;
    private String password;
    private String name;
    private Collection<ListModel> listsByUserId;


    public UserModel updateUser(UserModel updatedUser) {
        this.login = updatedUser.login;
        this.password = updatedUser.password;
        this.name = updatedUser.name;
        return this;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.TABLE)
    @Column(name = "userid", nullable = false)
    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    @Basic
    @Column(name = "login", nullable = false, length = 50)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 25)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserModel that = (UserModel) o;

        if (userid != that.userid) return false;
        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (userid ^ (userid >>> 32));
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "userByUserId", fetch=FetchType.LAZY)
    @Cascade({CascadeType.ALL})
    @JsonManagedReference
    public Collection<ListModel> getListsByUserId() {
        return listsByUserId;
    }

    public void setListsByUserId(Collection<ListModel> listsByUserId) {
        this.listsByUserId = listsByUserId;
    }
}
