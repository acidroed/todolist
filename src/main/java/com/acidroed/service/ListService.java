package com.acidroed.service;

import com.acidroed.entity.ListModel;
import com.acidroed.repository.ListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ListService {


    @Autowired
    private ListRepository listRepository;


    public void addList(ListModel list){
        listRepository.save(list);
    }

    public ListModel getListById(long id){
        return listRepository.findOne(id);
    }

    public void deleteListById(long id){
        listRepository.delete(id);
    }

    public ListModel updateList(ListModel list){
        ListModel oldList = listRepository.findOne(list.getListid());

        oldList.updateList(list);

        return listRepository.save(oldList);

    }
}
