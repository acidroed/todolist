package com.acidroed.service;

import com.acidroed.entity.ListModel;
import com.acidroed.entity.UserModel;
import com.acidroed.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserService {


    @Autowired
    private UserRepository userRepository;

    public Collection<UserModel> getAllUsers(){
        return this.userRepository.findAll();
    }

    public void addUser(UserModel user){
        userRepository.save(user);
    }

    public UserModel getUserById(long id){
        return userRepository.findOne(id);
    }

    public void deleteUserById(long id){
        userRepository.delete(id);
    }

    public UserModel updateUser(UserModel user){
        UserModel oldUser = userRepository.findOne(user.getUserid());

        oldUser.updateUser(user);

        return userRepository.save(oldUser);

    }

    public Collection<ListModel> getAllUsersLists(long userId){
        return userRepository.findOne(userId)
                .getListsByUserId();
    }
}
