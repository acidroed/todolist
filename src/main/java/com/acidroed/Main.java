package com.acidroed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@EntityScan(basePackages = {
        "com.acidroed.entity"
})
@EnableJpaRepositories(basePackages = {
        "com.acidroed.repository"
})
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }


}
