package com.acidroed.repository;

import com.acidroed.entity.UserModel;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserModel, Long> {

    List<UserModel> findAll();

    UserModel findOne(long id);

}
