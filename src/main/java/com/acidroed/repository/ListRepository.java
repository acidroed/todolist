package com.acidroed.repository;

import com.acidroed.entity.ListModel;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListRepository extends PagingAndSortingRepository<ListModel, Long> {

    ListModel findOne(long id);

}
