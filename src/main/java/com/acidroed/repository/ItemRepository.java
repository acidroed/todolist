package com.acidroed.repository;

import com.acidroed.entity.ItemModel;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends PagingAndSortingRepository<ItemModel, Long> {

    ItemModel findOne(long id);

}
